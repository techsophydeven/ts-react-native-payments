# ts-react-native-payments

Common 3rd party payments for react native using hooks

## Installation

```sh
npm install ts-react-native-payments
```

### Install Peer Dependencies

```sh
yarn add react-native-inappbrowser-reborn react-native-razorpay @stripe/stripe-react-native
```

## Usage

### Razorpay

* Register for razorpay and get the key_id
* Initialize the useRazorpayPayment hook with the key_id
``` js
  const {processPayment} = useRazorpayPayment(
    RAZORPAY_KEYS.key_id
  );
```

Get the Order ID from the server and use the processPayment method

```js
processPayment(parseFloat(amount), {
  description: 'Description',
  order_id: order_id_from_server,
  prefil: {
    email: 'sample@example.com',
    contact: '9876543210',
    name: 'John Doe',
  },
  theme: '',
  image: '',
  currency: 'INR',
  name: '',
});
```


### Paypal

* Register for paypal developer account
* Initialize the ```usePaypalPayment``` hook and specify test or not as boolean
``` js
  const {processPayment} = usePaypalPayment(true);
```

Get the Order ID from the server and use the processPayment method

```js
await processPayment(paypalResponse.data.order_id);
```


### Stripe

Register for stripe and get the publishable_key
``` js
  const {processPayment} = useStripePayment({
    publishableKey: "publishable_key_from_stripe",
  });
```

Get the paymentIntent, customerId and ephereralKey from the server and use the processPayment method

```js
await processPayment({
  merchantDisplayName: 'Techsophy',
  customerId: stripeResponse.data.customer,
  paymentIntentClientSecret: stripeResponse.data.paymentIntent,
  customerEphemeralKeySecret: stripeResponse.data.ephemeralKey,
});
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
